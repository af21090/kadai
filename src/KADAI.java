import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class KADAI {
    int sum=0;
    int sum1=0;
    private JPanel root;
    private JButton Unagi;
    private JButton Maguro;
    private JButton Samon;
    private JButton Uni;
    private JButton Ikura;
    private JButton Ebi;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JLabel textPane2;

    public KADAI() {
        Maguro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Maguro", 400);
            }
        });

        Maguro.setIcon(new ImageIcon(
                this.getClass().getResource("Maguro.jpg")
        ));

        Samon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Samon", 350);
            }
        });

        Samon.setIcon(new ImageIcon(
                this.getClass().getResource("Samon.jpg")
        ));

        Ikura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ikura", 500);
            }
        });

        Ikura.setIcon(new ImageIcon(
                this.getClass().getResource("Ikura.jpg")
        ));

        Unagi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Unagi",700);
            }
        });

        Unagi.setIcon(new ImageIcon(
                this.getClass().getResource("Unagi.jpg")
        ));

        Uni.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Uni", 550);
            }
        });

        Uni.setIcon(new ImageIcon(
                this.getClass().getResource("Uni.jpg")
        ));

        Ebi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ebi", 200);
            }
        });

        Ebi.setIcon(new ImageIcon(
                this.getClass().getResource("Ebi.jpg")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to check out ?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is" + sum + "yen");
                    textPane1.setText("");
                    textPane2.setText("total 0 yen");
                    sum=0;
                }
            }
        });
    }

    void order(String food, int price) {
        //int sum=0;
        sum+=price;
        int confirmation=JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0) {
            String currentText = textPane1.getText();
            textPane1.setText(currentText +"\n" + food+ " " + price + "yen");
            textPane2.setText("total " + sum + "yen");
            JOptionPane.showMessageDialog(null, "Order for " + food + " received");
            sum1++;
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrder");
        frame.setContentPane(new KADAI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}